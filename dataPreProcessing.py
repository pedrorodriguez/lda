import sys
import os
import re

'''
Takes two arguments: The first one is the the path of 
the directory containing the .txt files. The second is the 
the name of the file where the data is going to be stored
'''
def process(directory, filename, n):
  file_paths = []
  for root, dirs, files in os.walk(directory):
    for file in files:
      if file.endswith(".txt"):
        file_paths.append(os.path.join(root, file))

  new_file = open(filename + '.txt', 'w')

  i = 0
  while i < int(n): 
    for path in file_paths:
      count = 0
      with open(path,'r') as f:
        for line in f:
            line = line.lower()
            match = re.findall('[a-z]{2,}', line)
            for word in match:
              if count == 500:
                break
              new_file.write(word)
              new_file.write(' ')
              count+=1
      new_file.write(str(i))
      new_file.write('\n')
    i+=1


def word_count(file_path):
  count = 0
  with open(file_path,'r') as f:
    for line in f:
      line = line.lower()
      match = re.findall('[a-z]+', line) 
      for word in match:
        count += 1
  print(count)

def main(argv=None):
  process(sys.argv[1], sys.argv[2], sys.argv[3])


if __name__ == "__main__":
    sys.exit(main())