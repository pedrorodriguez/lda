% THIS IS SIGPROC-SP.TEX - VERSION 3.1
% WORKS WITH V3.2SP OF ACM_PROC_ARTICLE-SP.CLS
% APRIL 2009
%
% It is an example file showing how to use the 'acm_proc_article-sp.cls' V3.2SP
% LaTeX2e document class file for Conference Proceedings submissions.
% ----------------------------------------------------------------------------------------------------------------
% This .tex file (and associated .cls V3.2SP) *DOES NOT* produce:
%       1) The Permission Statement
%       2) The Conference (location) Info information
%       3) The Copyright Line with ACM data
%       4) Page numbering
% ---------------------------------------------------------------------------------------------------------------
% It is an example which *does* use the .bib file (from which the .bbl file
% is produced).
% REMEMBER HOWEVER: After having produced the .bbl file,
% and prior to final submission,
% you need to 'insert'  your .bbl file into your source .tex file so as to provide
% ONE 'self-contained' source file.
%
% Questions regarding SIGS should be sent to
% Adrienne Griscti ---> griscti@acm.org
%
% Questions/suggestions regarding the guidelines, .tex and .cls files, etc. to
% Gerald Murray ---> murray@hq.acm.org
%
% For tracking purposes - this is V3.1SP - APRIL 2009

\documentclass{acm_proc_article-sp}
\usepackage{algpseudocode} 
\renewcommand{\algorithmicforall}{\textbf{for each}}
\begin{document}

\title{Latent Dirichlet Allocation with Spark}

%
% You need the command \numberofauthors to handle the 'placement
% and alignment' of the authors beneath the title.
%
% For aesthetic reasons, we recommend 'three authors at a time'
% i.e. three 'name/affiliation blocks' be placed beneath the title.
%
% NOTE: You are NOT restricted in how many 'rows' of
% "name/affiliations" may appear. We just ask that you restrict
% the number of 'columns' to three.
%
% Because of the available 'opening page real-estate'
% we ask you to refrain from putting more than six authors
% (two rows with three columns) beneath the article title.
% More than six makes the first-page appear very cluttered indeed.
%
% Use the \alignauthor commands to handle the names
% and affiliations for an 'aesthetic maximum' of six authors.
% Add names, affiliations, addresses for
% the seventh etc. author(s) as the argument for the
% \additionalauthors command.
% These 'additional authors' will be output/set for you
% without further effort on your part as the last section in
% the body of your article BEFORE References or any Appendices.

\numberofauthors{2} %  in this sample file, there are a *total*
% of EIGHT authors. SIX appear on the 'first-page' (for formatting
% reasons) and the remaining two appear in the \additionalauthors section.
%
\author{
% You can go ahead and credit any number of authors here,
% e.g. one 'row of three' or two rows (consisting of one row of three
% and a second row of one, two or three).
%
% The command \alignauthor (no curly braces needed) should
% precede each author name, affiliation/snail-mail address and
% e-mail address. Additionally, tag each line of
% affiliation/address with \affaddr, and tag the
% e-mail address with \email.
%
% 1st. author
\alignauthor
Pedro Rodriguez\\
       \affaddr{UC Berkeley}\\
       \email{ski.rodriguez@gmail.com}
% 2nd. author
\alignauthor
Arturo Pacifico Griffini\\
       \affaddr{UC Berkeley}\\
       \email{pacifico.arturo@gmail.com}
}

\maketitle
\begin{abstract}
In this paper we present a parallel implementation of latent dirichlet allocation for topic modeling in Spark.
The growth of big data and high performance computing has over the years contributed the study and development
of different models of computation. In this project we focus on using the MapReduce motif \cite{motifs} to create
a solution to latent dirichlet allocation. Prior work has been done by Google Beijing \cite{plda} with implementations
in Hadoop and MPI written and analyzed. In this paper we discuss the implementation of LDA with a newer MapReduce
framework called Spark which allows for higher degrees of efficiency with reduced communication. We also discuss
some of the programming benefits of using Spark while exploring the challenges encountered.
\end{abstract}

\category{D.2.0}{Software Engineering}{General}

\terms{Performance, Algorithms}

\keywords{LDA, PLDA, Spark, MapReduce, Topics Model} % NOT required for Proceedings

\section{Introduction}
Topic modeling first emerged in 1998 with LDA being developed by Blei, Ng, and Jordan in 2003 \cite{lda}. In general
topic modeling with LDA is posing the question: given a corpus composed of a set of $D$ documents, what is the composition of topics present in those
documents? Furthermore, which words are most strongly associated with each topic. 
For more details on the algorithm we defer to the paper by Blei et all \cite{lda} and Wang \cite{plda} which in detail
explain how LDA works. To implement parallel LDA, we chose to use Spark, an emerging open source framework for data
analytics in distributed settings created by the AMPLab at UC Berkeley. In this project our goal was to improve on the
performance achieved by the Hadoop implementation by Wang \cite{plda} and more closely match the MPI implementation which
stores documents locally, as opposed to Hadoop which does not.

\subsection{Spark}

Spark is a powerful MapReduce framework which exposes a new feature not previously available in Hadoop: caching.
This dramatically reduces the number of read and write to disk and makes Spark able to execute batch-processing jobs 10 to 100 times faster than Hadoop. In a typical MapReduce job on Hadoop, after the map and reduce tasks there is a synchronisation barrier and all the data is persisted to disk. While this feature was designed to allow a job to be recovered in case of hardware failure, it did not allow to fully leverage the memory of the cluster. On the other hand, Spark has no synchronisation barrier to slow the jobs down. It achieves this with this concept of Resilient Distributed Dataset, which allows you to store data on memory and persist it to disk if needed. This characteristic, makes Spark extremely efficient for iterative algorithms and hence, can be leveraged by machine learning and graph algorithms.

Another advantage of Spark is its ease of use. Having used Hadoop previously, Spark is
much more user friendly and the codebase has significantly less overhead. Spark
certainly fits the class of software initiatives that strive to increase programmer
productivity while maintaining or improving performance.

\subsection{Motivation}
With the growth of Spark, has also come the growth of an accompanying package called MLLib which supports many machine
learning algorithms. This is natural because the cases where Spark improves performance the most are in iterative algorithms
on the same data set, a common pattern in machine learning. Since there was no existing implementation for topic models, and
LDA in particular, we decided to attempt our own implementation. With the sizes of the data sets for LDA reaching
multiple Gigabytes or Terabytes (such as Wikipedia or the entire web), it is a natural choice for distributed computing
with MapReduce. Our motivation for this project is to implement LDA with Spark to allow for analysis of large corpuses
and eventually integrate it into the Spark/MLLib codebase. In order to justify this, our goal was to improve on the
performance given by Hadoop implementations which cannot take advantage of the caching that Spark is capable of.




%----------------------------------------------------------------------------------------
%	ALGORITHM DETAILS  Pedro
%----------------------------------------------------------------------------------------

\section{Algorithm} 

LDA was first described by Blei, Ng, Jordan in 2003 \cite{lda}. There are four primary
components of the algorithm: initialization, reconstruction and calculation of matrices,
Gibbs Sampling for topics, and generation of the inferred distributions after the final
iteration. In this section we present our algorithm and step through a runtime analysis.
For a walkthrough of LDA in general with an example, we strongly suggest looking at the illustrative
example in section 3.2 of Wang\cite{plda}.

\subsection{Variables and Constants}
As input, we receive a set of $D$ documents labeled $d_i$. Each document contains any number
of words labeled $w_{i|d}$, the $ith$ word in document $d_i$. In this set of documents, we
find a vocabulary of $V$ words which represents all unique words present in the documents.
We define as an input $K$, the number of topic assignments possible. Using these definitions,
we construct a matrix $C^{word}$ which is $V\times K$ large. This matrix contains the
counts of assignments to each of $K$ possible topics for each word in the vocabulary.
$C^{doc}$ is defined as a $D\times K$ matrix. This contains for each document, the counts
of word assignments to each of $K$ possible topics. As output, there are two distributions.
The first, $\theta_{d,k}$ represents the topic mixture of documents. The second,
$\Phi_{v,k}$ represents the distribution of each word in the vocabulary per topic.

\subsection{High Level Overview}
Our algorithm is comprised of four primary phases: initialization, computation of $C^{word}$,
Gibbs Sampling, and calculation of $\theta_{d,k}$ and $\Phi_{v,k}$.
\subsubsection{Initialization}
In this portion of the algorithm, we receive the input corpus: a set of $D$ documents
labeled $d_i$, each containing words labeled $w_{i|d}$. First, we determine the vocabulary
present in the $D$ documents and assign a variable $V$ to hold its size. We then
iterate through each word in each document and randomly assign each word one of $K$ topics
randomly from a uniform distribution. Additionally, we map each word to a unique number
from $0$ to $V-1$, rather than emit the actual word.
\subsubsection{Computation of $C^{word}$}
This phase of the algorithm receives output either from the initialization phase or the
Gibbs sampling phase following the end of an iteration. The input to this phase is a
list of tuples with key being the number representing each word and the value the number
representing the topic assignment. These results are tabulated to compute the $C^{word}$
matrix.
\subsubsection{Gibbs Sampling}
In this phase we use the result of computing $C^{word}$ and the current word-topic tuple
assignment pairs to generate new assignments via Gibbs sampling to each word in each document.
This is a process done on a per document basis. After receiving a document and its associated
word-topic pairs, the associated row of $C^{doc}$ is computed. Then, the topic of each
word is resampled according to the following distribution over $K$ possible topics:\\
\begin{align*}
p(z_{d,i}=k|w_{d,i}=v,Z_{-(d,i)},W_{-(d,i)})\propto\\
(C^{doc}_{d,k}+\alpha)\frac{C^{word}_{v,k}+\beta}{\sum_{v'}C^{word}_{v',k}+V\beta}
\end{align*}
Where $W_{-(d,i)}$ represents the words in the corpus with $w_{d,i}$ removed and
$Z_{-(d,i)}$ the corresponding topic assignments. $z_{d,i}$ represents the new topic
assignment for the $ith$ word in the document. $\alpha$ and $\beta$ are hyper parameters
of the ML algorithm. Once each word in each document is resampled, the output
is the input into the next iteration, or the algorithm proceeds to the final step.
\subsubsection{Calculation of $\theta_{d,k}$, $\Phi_{v,k}$, and Termination}
After a sufficient number of iterations, Gibbs sampling will converge. Since the focus
of this paper and the Wang\cite{plda} is on computational performance rather than machine
learning performance, we did not determine number of iterations needed to converge,
but instead benchmarked against $10$ iterations. In the phase, $C^{word}$ must be computed
one last time as well as each row vector in $C^{doc}$. These results are combined to
output:
\begin{align*}
\theta_{d,k}=\frac{C^{doc}_{d,k}+\alpha}{\sum_{k'=1}^K C^{doc}_{d,k'}+K\alpha}\\
\Phi_{v,k}=\frac{C^{word}_{v,k}+\beta}{\sum_{v'=1}^{V} C^{word}_{v',k}+V\beta}
\end{align*}

\subsection{Implementation with Spark MapReduce and Runtime Analysis}
Now we go into detail how each component was formulated as a MapReduce problem. In each
part, we discuss what are the key-value pairs emitted and how they are reduced. First
pseudo code is presented, then some discussion with runtime analysis.

\subsubsection{Initialization}
\vspace{10 mm}
\begin{algorithmic}[1]
    \Require $D$ documents $d_i$, $d_i=w_1,...,w_i,...$
    \State $d_i$=hashcode(words)
    \Procedure{VocabMap}{$d_i$, line}
        \State words = line.split(" ")
        \State \Return words
    \EndProcedure
    \State vocab = VocabMap(input).distinct().collect()
    \State V = size(vocab)
    \Procedure{TopicAssignMap}{$d_i$, line}
        \State words = line.split(" ")
        \State wordTopics = []
        \ForAll{word in words}
            \State topic = Rand from 0 to K - 1
            \State word = transform word string to int
            \State wordTopics.append(word, topic)
        \EndFor
        \State emit($d_i$, wordTopics)
    \EndProcedure
    \State document\_rdd = TopicAssignMap(input)
    \State document\_rdd.cache()
    
\end{algorithmic}
This part of the algorithm primarily servers to: establish a vocabulary, and assign random
initial topic assignments. Lets make some assumptions which we will carry throughout
the rest of this paper. First, $O(K)=O(1)$. This seems reasonable since $K$ is a user
defined constant over topics. Second, $O(D/W)$=$O(1)$. This means that the number of
words per document is also constant. This is reasonable since our word length should
not scale with our number of documents in any real way (statistically we will encounter
documents with more words with a larger sample, but we do not include this effect).
Furthermore, we can truncate documents to enforce this criterion if necessary. Now we
turn to a runtime analysis. The first procedure should be $O(W)$ since we consider each
word once as a potential new word in our vocabulary. This can be done using a dictionary
or a similar data structure. Since we do not grow past the size of the human language, lookup
time should remain roughly $O(1)$ since we do not reach significant collisions. The distinct
function is implemented by Spark and predictably is one of our fastest actions. Thus overall
runtime is $O(W)=O(D)$, or linear in the number of input words and thus documents.

In the second portion, we also are linear with documents. We consider each word once,
then assign a topic. This is the same runtime since rather than checking a dictionary,
we simply add the term to a list then emit the document hash and list. Thus runtime is
again $O(D)$.
\subsubsection{Computation of $C^{word}$}
\vspace{10 mm}
\begin{algorithmic}[1]
    \Require document\_rdd
    \Procedure{BreakDocumentMap}{$d_i$, wts=List[$(w_i, t)$]}
        \State wts.foreach(wt => emit(word, topic))
    \EndProcedure
    \State wordTopics = map(input => BreakDocumentMap(input))
    \State wordTopics = wordTopics.groupByKey()
    \Procedure{WordTopicVectorMap}{word,List[Topic]}
        \State vector = $K\times 1$ vector
        \ForAll{topic in topics}
            \State vector(topic) += 1
        \EndFor
        \State emit(word, vector)
    \EndProcedure
    \State vectors = WordTopicMap(wordTopics)
    \Procedure{WordTopicMatrixMap}{word,Vector}
        \State matrix = $K \times V$ zero matrix
        \State i = word
        \State matrix(ith row) = vector
        \State emit matrix
    \EndProcedure
    \State $C^{word}=$WordTopicMatrixMap(vectors).reduce(\_ + \_)
    
\end{algorithmic}
The role of this algorithm is to precompute the $C^{word}$ matrix necessary for Gibbs
updates and the final output. As input, the initial maps receive a document as input
with its associated word-topic tuples. These are first broken apart into individual
tuples of word-topic and emitted. We then use a directive of Spark to group these by
key. We want for each word in the vocabulary, tabulate its topic counts. To reduce
communication and garbage collection overhead, we do this in place with a accumulation
vector. To prevent this particular job from becoming very large, we use in code
additional Spark syntax to partition large lists onto multiple nodes. We again, are running
through every word once but this time adding its topic to a vector. This is $O(W)$
and therefor $O(D)$.


Our second step is to place this vectors in empty matrices, then reduce them through
a combination of local accumulation (in code, not shown here), and with Spark's reduce
operator. The terms inside the reduce call represent adding the two input elements, an
operation defined for matrices. The maximum number of such matrices is $V$ if every
word is unique. Thus, $O(W)>O(V)$. The subsequent reduction can be done via divide
and conquer to achieve a logarithmic runtime in $V$. So the overall runtime is
$O(W)+O(logV)=O(D)+O(logW)=O(D)$. The assignment to vectors is linear and the summation
is logarithmic, which is overall reducible to linear in documents.
\subsubsection{Gibbs Sampling}
\vspace{10 mm}
\begin{algorithmic}[1]
    \Require document\_rdd, $C^{word}$
    \Procedure{GibbsMap}{$(d_i, wts=List[(w_i,t_i)])$}
        \State $C^{doc}_d$ = $K\times 1$ vector
        \ForAll{$(w_{i}, t_i)$ in wts}
            \State $C^{doc}_d(t_i)$ += 1
        \EndFor
        \ForAll{wt=$(w_{i}, t_i)$ in wts}
            \State $C^{doc}_d(t_i)$ -= 1
            \State $C^{word}(w_i, t_i)$ -= 1
            \State topic = Draw new topic
            \State $C^{doc}_d(topic)$ -= 1
            \State $C^{word}(w_i, topic)$ -= 1
            \State wt = $(w_i,topic)$
        \EndFor
        \State emit($d_i$,$wts$)
    \EndProcedure
    \State document\_rdd = GibbsMap(document\_rdd)
    
\end{algorithmic}
In this phase we do the Gibbs sampling central to this implementation of LDA. This map
function takes in the document\_rdd created before, and returns a new one as output which
is used as input on the next iteration. This is one place we take advantage of Spark
caches. We compute a document\_rdd, but can obtain two computations from it: computing
$C^{word}$ and the new document\_rdd.

Now for runtime. The calculation of $C^{doc}$ is linear in $W$ since we require iterating
over each word once. Next, the computation of the sum on the denominator needs to be
only done once per iteration of the algorithm, which in our code is done via calls
to the included linear algebra package, thus contributes a factor of $O(KlogV)<O(logW)$.
Thus it does not dominate runtime. Lastly, to do the actual Gibbs sampling, we must run
over all words again, which is $O(W)=O(D)$ again. Thus, thus phase is $O(W)=O(D)$ runtime.

\subsubsection{Calculation of $\theta_{d,k}$, $\Phi_{v,k}$, and Termination}
For this section we do not present an algorithm since it does not contribute to runtime.
In this phase, the most expensive portion is the recalculation of the $C^{word}$ matrix.
In our final code implementation, this is currently not an output due to focus on runtime.
This is part of our future work as we begin to take steps to merge our code into the Spark
main branch as part of MLLib.
\subsubsection{Full Algorithm}
\vspace{10 mm}
\begin{algorithmic}[1]
    \State Initialize
    \ForAll{i in ITERATIONS}
        \State Compute $C^{word}$
        \State Do Gibbs Sampling
    \EndFor
    \State Calculate $\theta$ and $\Phi$
\end{algorithmic}

\subsubsection{Runtime Analysis Conclusion}
Since all parts of our algorithm are $O(W)=O(D)$, our algorithm has linear runtime
with number of documents. This is confirmed empirically with our runtime experiments
discussed further in the report. This runtime is also embarrassingly parallel and therefor
scales well. It falls in this category because the smallest unit of work is the computation
of a full document. Since this work is fully independent given $C^{word}$, and $C^{word}$
can be computed in an embarrassingly parallel way, this creates the potential for
perfect scaling with the number of processors.

\subsection{Optimizations}
As is the case with most code, ours was far from perfect on our first try. In this section
we discuss problems which arose and our optimizations to fix them. The largest issue we
ran into was Java Garbage Collection and Heap Space Exceptions. This indicated that we
were creating new objects at a rate fast enough to significantly slow down our algorithm
and in many cases crash it.
Most of the optimizations which follow were attempts at curbing this problem.

\subsubsection{Object Serialization}
Native java object serialization is quite bad, both in runtime and memory consumption.
This was naturally a place where we could easily obtain a very large constant factor
improvement. We decided to use Kryo serialization, which performs better than Java in both runtime and memory. Kryo benchmarks are shown in Figure \ref{fig:rttserial} and Figure
\ref{fig:memserial}. Overall, this optimization made a large difference in the performance of our implementation.

\begin{figure}[h]
\centering
\includegraphics[scale=.44]{rtt-java}
\caption{Kryo vs Java Serialization Runtime}
\label{fig:rttserial}
\end{figure}
\begin{figure}[h]
\centering
\includegraphics[scale=.44]{size-java}
\caption{Kryo vs Java Serialization Memory}
\label{fig:memserial}
\end{figure}

\subsubsection{Spark Cache}
Spark allows for use of caching results of actions on individual nodes. In the context
of our algorithm, this is particularly relevant to the variable document\_rdd. This variable
is calculated, then used as inputs to two different operations. Had we used Hadoop, the
results of the first operation would have to be sent twice, therefor using twice the
communication bandwidth or being computed twice, which is even worse. Spark allowed us
to cache the output to each node, then reuse that output to thereby decrease communication
and computation. We did not empirically test this, but we estimate a 2x speed boost
based on runs with more primitive versions of the code.

\subsubsection{Spark MapPartitions}
When running into Java GC issues, the primary suggestion we received was to transition
from using regular Spark Map calls to MapPartition. The issue with regular map calls for
methods which created new objects, was that for each input, a new object was created, then
collected by GC. MapPartitions allow for reuse of a single object across a reasonable size
partitions. Regular map effectively works with the size of a partition set to 1. Below
is an excerpt from our code which illustrates the change:
\begin{verbatim}
input.mapPartitions(partition => {
    val m = DenseMatrix.zeros[Int](l_V, l_K)
    partition.foreach(kv => {
        val key = kv._1
        val v = kv._2
        m(key, ::) :+= v.t
})
\end{verbatim}


The above code sample shows that rather than create a new full matrix per vector, we
can use partitions to accumulate the result over partition, then emit the matrix, therefor
reducing our garbage by a factor proportional to $1/partitions$. This variable
is set by Spark to be the optimal value depending on number of compute nodes available.

\subsubsection{Dictionary Optimization}
The last and consequently final optimization we made was related to dictionary use. We had
previously noted that our runtime per iteration increased by about 10-20\%. At the same
time, we had been using Strings as our keys for words, then using a lookup table
from String words to a unique integer mapping between $0$ and $V-1$ to compute
$C^{word}$ with. We do not think this was very large and observed its size through the
Spark panel as it was communicated from the slaves to the master. Our final optimization
was to immediately upon determining the vocabulary, switch the key of words to the unique
integer representation rather than String. We predicted this would give a constant factor
improvement on runtime, due to removing the need to serialize the dictionary and the constant
time lookups. To our surprise, not only did the runtime improve significantly, but our
problems with runtime increase per iteration vanished. We have no explanation for this
behavior, but hope to discover its cause as we work to integrate into MLLib. We surmise
perhaps that the serialization of the dictionary involved many pointers so the overhead
is perhaps very large.

\subsubsection{Conclusion and Future Work}
The optimizations presented made it possible to test up to data sets of 512MB. This is
a great improvement over not being able to handle small 10MB files with our completely
unoptimized version of code. As we look to incorporate into MLLib, we would like to begin
taking looks at possible alternative implementations which make a tradeoff of more computation
for using less memory and less object creation. Although it is very possible that there
are more optimizations to be made for GC in this version of code, we believe its possible
that we have encountered a wall which requires rethinking our implementation to get to even
larger data sets. Our exploration will be predominantly into algorithms which focus on
communicating only differences from the current state as opposed to communicating all
assignments.

%----------------------------------------------------------------------------------------
%	PERFORMANCE  
%----------------------------------------------------------------------------------------

\section{Performance Results}
In this section we will specifically discuss the results of our  implementation and its performance for different problem sizes and number of processors. We discuss weak  scaling, which checks if by increasing problem size and number of processors by the same ratio keeps the work per processor ratio the same. We also discuss strong scaling which keeps the problem size the same and varies the number of processors, checking to see if efficiency is maintained.

Due to time limitations, and the desire to keep EC2 costs low, we ran the implementation for each data/nodes configuration just once, with the assumption in mind that the runtime deviation for all configurations is small. For a more comprehensive benchmark we would have run the same configuration multiple times 

\subsection{Data set}
We tested the Spark implementation of LDA on data sets of sizes 32MB, 64MB, 128MB, 256MB, and 512MB. These files are composed by collections of documents taken from the Neural Information Processing Systems Foundations. All the documents in each dataset have an average of 500 words. The size of the dataset grows proportionally with the number of documents in the file. The 512MB dataset is composed of about 160000 documents. 

\subsection{Empirical runtime analysis}
We kept the number of words constant to 500 per document for all the datasets, and we ran our LDA implementation on 1 node to prove that the runtime is linear in the number of documents as predicted by the runtime analysis. The results displayed in figure \ref{fig:runtime1node} confirms that our implementation is linear in the number of documents. 

\begin{figure}[h]
\centering
\includegraphics[scale=.44]{serialRunningTime.png}
\caption{Shows the serial running time of LDA on variable number of documents with fixed average word count per document.}
\label{fig:runtime1node}
\end{figure}

We get the same scaling results for multiple nodes, which is shown in figure \ref{fig:runtime_multi_nodes}.

\begin{figure}[h]
\centering
\includegraphics[scale=.44]{serialMultiNodes.png}
\caption{Shows the serial running time of LDA on variable number of documents with fixed average word count per document.}
\label{fig:runtime_multi_nodes}
\end{figure}


\subsection{Weak scaling}
In the case of weak scaling, the problem size and number of processors available to the job are both increased. For our implementation we expect to see a linear relationship between the runtime and the number of nodes with slopes slightly greater than zero. We expect this divergence from ideal weak scaling because of the communication overhead brought by parallelisation. Also, we expect to see steeper slopes for greater problem sizes per node, since the greater the data size per node the more data needs to be exchanged between the nodes.  Figure \ref{fig:weak_scaling_linear_plot} shows weak scaling plots for three different situations. Namely, when the data sizes per node are kept constant at 16MB, 32MB, and 64MB. 


\begin{figure}[h]
\centering
\includegraphics[scale=.44]{weakScaling.png}
\caption{Shows weak scaling for 3 different fixed problem sizes per node.}
\label{fig:weak_scaling_linear_plot}
\end{figure}

The results conform with our expectations. We notice linear scaling for the 16MB, and 32MB per node cases with the latter having a steeper slop than the former. For the 64MB per node case, we do not have enough data points to be sure. The first and the two last data points suggest a linear relationship with slope similar to the 32MB per node case. Perhaps, the divergence from linear scaling in the 64MB per node case, is due to the statistical fluctuations caused by running just once per size/nodes configuration.

\subsection{Strong scaling}
The second type of desired performance is strong scaling. This holds the problem size constant and increases the number of processors. The objective is to keep the efficiency the same for increasing numbers of processors. We define efficiency as $E=S/N$, where $S$ is the speedup over the serial version and $N$ is the number of nodes used in the computation. Figure \ref{fig:strong_scaling_plot} shows the strong scaling plots for the runtime vs the number of nodes for a fixed problem size. Whereas, figure \ref{fig:strong_scaling_efficiency} shows strong scaling plots for the efficiency, as defined before, vs the number of nodes for fixed problem sizes. 

\begin{figure}[h]
\centering
\includegraphics[scale=.44]{strongScaling.png}
\caption{Shows strong scaling for different problem sizes.}
\label{fig:strong_scaling_plot}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[scale=.44]{strongScalingEfficiency.png}
\caption{Shows how efficiency is affected by the number of nodes for different problem sizes.}
\label{fig:strong_scaling_efficiency}
\end{figure}

We can infer from figure \ref{fig:strong_scaling_efficiency} and figure  \ref{fig:strong_scaling_plot} that larger problem sizes stay close to the ideal linear scaling line longer than smaller sizes. This is due to the fact that for small problem sizes running ,on many nodes we don't fully harness the total memory of the cluster, as the amount of data per node is small compared to the nodes memory. In such case, the  communication overhead of adding new nodes outweighs the parallelisation benefits. The efficiency begins to quickly decay when the data size per node gets smaller than 32MB/node. 


\section{Conclusion and future work}
In the pursuit of our original goal: achieve better performance than Hadoop, we ran into
numerous challenges with Spark and EC2, many of which we overcame, some which we would
like to continue exploring solutions to. 

\subsection{Performance}
In terms of performance, we did not meet all our objectives, but achieve expected results
in some areas and promising results in the others. Our scaling with data sizes was as
expected, linear with documents of constant size. Our weak scaling goal was hopefully
a flat line representing that with a double in nodes and data, runtime remains the same.
Our weak scaling is fairly good with a small slope which appears to be flattening further
with increased number of data size per node. This represents achieving optimal efficiency
as we saturate each node in memory and computational work.

Our last performance goal was strong scaling. In this area we did not achieve our hope, but
based on the data are confident that as we improve our program to handle larger data sizes
(less GC overhead), nodes will begin to be fully saturated with data and reach peak
efficiency. This can be seen in Figure \ref{fig:strong_scaling_plot} by noting that with
each increase in work per node, the nodes lose efficiency at greater number of nodes.
We believe that with continual improvement to eliminate GC issues, we can achieve very good
strong scaling. Getting rid of GC issues would allow us to saturate nodes and reduce costly
GC overhead which is consuming large portions of computation time. Overall, we believe
our performance results are a success and a good basis for future work.

\subsection{EC2}
To run our project, we were unfortunate in not being able to use the NERSC facilities since
they do not have Spark installed. Rather than go through the request process, we followed
suggestions from the Amplab and used EC2. We ran into issues with a bug introduced into the
EC2 scripts shortly before using it, which delayed us by two days. Other than this rather
large hiccup, running on EC2 was generally smooth once our code was correctly packaged.
Over the course of the project, EC2 costs came out to be about \$100 which seemed
reasonable for running our larger data sets and runtimes.

\subsection{Spark}
As our first foray into Spark (and Scala), our experience was generally positive, but fairly
rocky. While documentation in places can be a little scarce, Evan Sparks and Shivram Venkataraman from the Amplab were very helpful when we had problems and questions.
Based on our experience, we plan on writing an article, blog post, or contributing to
documentation on areas we did a little too much exploring.

One large underlying theme of this course has been the tradeoff between performance potential
and developer time. Spark/Scala hit a nice medium in this respect. There is a good number
of functions readily available for common tasks, but the framework is extensible enough
to provide allow for things such as custom accumulators, partitions splitting, and other
capabilities. As Spark grows and is better documented, it should see more common use for
its ease of use.

\subsection{MLLib}
Currently the codebase does not conform with requirements to contribute to Spark's Machine Learning library (MLlib). Our next objective will be to prepare the codebase for submission by adding documentation, tests, sensible output, input, and conforming to style conventions.

%\subsection{Spark streaming}
%We ran our the algorithm on datasets of size no greater than 1GB which we copied locally on each nodes. Rather than processing a batch of stored data after the fact, Spark can also manipulate data in real time with Spark Streaming. We are thinking to 

\subsection{DASMT}
In the course of looking for data sets to test on, we were in contact with a researcher
from Stanford. Although we have not had much time to followup, the researcher seemed
interested in perhaps leveraging our work by using LDA for
Domain Adaptation for Statistical Machine Translation. DASMT attempts to optimize machine translation for language in different domains. For example translating medical language versus colloquial language. Topic models such as LDA could be used to assist DASMT by providing
clues to correct, topic/context aware translations.

\subsection{Future Work}
This class project has been a great way to break into contributing to Spark, but there are
still many steps left before getting to that point. As part of our future work, we need
to continue optimizing the algorithm and potentially reformulating our updates to reduce
Garbage Collection problems. This is the primary bottleneck of our algorithm at present.
We plan on exploring algorithms which reduce the use of objects in favor of more computation.

The next step after overcoming this issue would be to begin running on large data sets and
number of nodes comparable to the Wang\cite{plda} paper to benchmark results against Hadoop,
something we weren't able to do in this project. This would pave the way for refactoring
our code to be integrated into MLLib as part of Spark.


%\end{document}  % This is where a 'short' article might terminate

%ACKNOWLEDGMENTS are optional
\section{Acknowledgments}
We wanted to thank Shivram Venkataraman, and James Demmel for their help, and especially Evan Sparks for his advice on setting up the EC2 clusters and his invaluable optimisation suggestions. 

\appendix

\bibliographystyle{abbrv}
\bibliography{report}

\balancecolumns
% That's all folks!
\end{document}
