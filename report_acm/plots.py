# CS267 report plots script
import matplotlib.pyplot as plt
nodes = [1, 2, 4, 8, 12, 16, 20, 24, 28, 32]
speedup32 = [1.00, 1.69, 2.33, 2.29, 2.82, 2.08, 2.29, 2.66, 2.67, 2.70]
speedup64 = [1.00, 1.99, 3.38, 4.51, 4.84, 4.27, 3.99, 4.28, 4.87, 5.06]
speedup128 = [1.00, 1.99, 3.72, 5.74, 7.18, 7.23, 7.39, 6.75, 7.49, 8.13]
speedup256 = [1.00, 1.98, 3.74, 6.68, 8.19, 9.94, 9.37, 10.14, 11.17, 11.96]
speedup512 = [1.00, 1.99, 3.97, 7.29, 10.14, 11.50, 11.94, 13.44, 14.05, 15.47]
plt.figure(1)
plt.plot(nodes, nodes)
plt.plot(nodes, speedup32)
plt.plot(nodes, speedup64)
plt.plot(nodes, speedup128)
plt.plot(nodes, speedup256)
plt.plot(nodes, speedup512)
plt.title('Strong Scaling')
plt.ylabel('Speedup')
plt.xlabel('Number of nodes')
plt.legend(['Ideal', '32MB', '64MB', '128MB', '256MB', '512MB'], loc=2)
plt.show()

#total dataset size vs execution time
sizes = [32, 64, 128, 256, 512]
time1 = [136.0, 279.0, 547.4, 1081, 2150]
time2 = [80.6, 140.4, 275.3, 547.2, 1079]
time4 = [58.3, 82.6, 147.3, 289.0, 541]
time8 = [59.3, 61.8, 95.3, 161.9, 295]
time12 = [48.2, 57.6, 76.2, 132.0, 212]
time16 = [65.3, 65.4, 75.7, 108.7, 187]
time20 = [59.5, 70.0, 74.1, 115.4, 180]
time24 = [51.1, 65.2, 81.1, 106.6, 160]
time28 = [50.9, 57.3, 73.1, 96.8, 153]
time32 = [50.4, 55.1, 67.3, 90.4, 139]
plt.figure(2)
plt.plot(sizes, time1)
plt.plot(sizes, time2)
plt.plot(sizes, time4)
plt.plot(sizes, time8)
plt.plot(sizes, time12)
plt.plot(sizes, time16)
plt.plot(sizes, time20)
plt.plot(sizes, time24)
plt.plot(sizes, time28)
plt.plot(sizes, time32)
plt.title('Tot dataset size vs execution time')
plt.ylabel('Time (s)')
plt.xlabel('Data Sizes (MB)')
plt.legend(['1 node', '2 nodes','4 nodes','8 nodes','12 nodes','16 nodes','20 nodes', '24 nodes', '28 nodes', '32 nodes'], loc=2)
plt.show()


plt.figure(3)
w_time64 = [279.0, 275.3, 289, 295]
w_time32 = [136.0, 140.4, 147.3, 161.9, 187]
w_time16 = [80.6, 82.6, 95.3, 108.7, 139]
plt.plot([1, 2, 4, 8], w_time64)
plt.plot([1, 2, 4, 8, 16], w_time32)
plt.plot([2, 4, 8, 16, 32], w_time16)
plt.title('Weak Scaling')
plt.ylabel('Time (s)')
plt.xlabel('Number of nodes')
plt.legend(['64MB per node', '32MB per node', '16MB per node'], loc=1)
plt.show()